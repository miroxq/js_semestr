import { kinopoiskAPI } from "./API.mjs";

const kinopoiskapiunofficial = new kinopoiskAPI();
let moviesData = await kinopoiskapiunofficial.getMovies(kinopoiskapiunofficial.API_URL_POPULAR);

let greetBtn = document.getElementById('greet');
greetBtn.addEventListener("click", () => {
  const header = document.getElementsByClassName("container")[0];
  let greatingText = document.createElement("a");
  var inputValue = document.getElementById("name").value;
  greatingText.textContent = "Здравствуйте, " + inputValue + "!";
  greatingText.style.color = "white"
  header.appendChild(greatingText);
  showMovies(moviesData);

  let greetingsDiv = document.getElementById("greetings");
  greetingsDiv.remove();
})

function getClassByRate(vote) {
  if (vote >= 7) {
    return "green";
  } else if (vote > 5) {
    return "orange";
  } else {
    return "red";
  }
}

function showMovies(data) {
  const moviesEl = document.querySelector(".movies");

  // Очищаем предыдущие фильмы
  document.querySelector(".movies").innerHTML = "";

  data.films.forEach((movie) => {
    const movieEl = document.createElement("div");
    movieEl.classList.add("movie");
    movieEl.innerHTML = `
        <div class="movie__cover-inner">
        <img
          src="${movie.posterUrlPreview}"
          class="movie__cover"
          alt="${movie.nameRu}"
        />
        <div class="movie__cover--darkened"></div>
      </div>
      <div class="movie__info">
        <div class="movie__title">${movie.nameRu}</div>
        <div class="movie__category">${movie.genres.map(
          (genre) => ` ${genre.genre}`
        )}</div>
        ${
          movie.rating &&
          `
        <div class="movie__average movie__average--${getClassByRate(
          movie.rating
        )}">${movie.rating}</div>
        `
        }
      </div>
        `;
    movieEl.addEventListener("click", () => openModal(movie.filmId))
    moviesEl.appendChild(movieEl);
  });
}

const form = document.querySelector("form");
const search = document.querySelector(".header__search");

form.addEventListener("submit", async (e) => {
  e.preventDefault();

  const apiSearchUrl = `${kinopoiskapiunofficial.API_URL_SEARCH}${search.value}`;
  if (search.value) {
    let searchData = await kinopoiskapiunofficial.getMovies(apiSearchUrl);
    showMovies(searchData);
    search.value = "";
  }
});

// Modal
const modalEl = document.querySelector(".modal");

async function openModal(id) {
  
  const respData = await kinopoiskapiunofficial.getMovies(kinopoiskapiunofficial.API_URL_MOVIE_DETAILS + id);
  
  modalEl.classList.add("modal--show");
  document.body.classList.add("stop-scrolling");

  modalEl.innerHTML = `
    <div class="modal__card">
      <img class="modal__movie-backdrop" src="${respData.posterUrl}" alt="">
      <h2>
        <span class="modal__movie-title">${respData.nameRu}</span>
        <span class="modal__movie-release-year"> - ${respData.year}</span>
      </h2>
      <ul class="modal__movie-info">
        <div class="loader"></div>
        <li class="modal__movie-genre">Жанр - ${respData.genres.map((el) => `<span>${el.genre}</span>`)}</li>
        ${respData.filmLength ? `<li class="modal__movie-runtime">Время - ${respData.filmLength} минут</li>` : ''}
        <li >Сайт: <a class="modal__movie-site" href="${respData.webUrl}">${respData.webUrl}</a></li>
        <li class="modal__movie-overview">Описание - ${respData.description}</li>
      </ul>
      <button type="button" class="modal__button-close">Закрыть</button>
    </div>
  `
  const btnClose = document.querySelector(".modal__button-close");
  btnClose.addEventListener("click", () => closeModal());
}

function closeModal() {
  modalEl.classList.remove("modal--show");
  document.body.classList.remove("stop-scrolling");
}

window.addEventListener("click", (e) => {
  if (e.target === modalEl) {
    closeModal();
  }
})

window.addEventListener("keydown", (e) => {
  if (e.keyCode === 27) {
    closeModal();
  }
})

const logo = document.getElementsByClassName("header__logo")
logo[0].addEventListener("click", () => {
  showMovies(moviesData)
})