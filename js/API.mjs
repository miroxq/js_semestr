class kinopoiskAPI 
{
    constructor ()
    {
        this.API_KEY = "8c8e1a50-6322-4135-8875-5d40a5420d86";
        this.API_URL_POPULAR = "https://kinopoiskapiunofficial.tech/api/v2.2/films/top?type=TOP_100_POPULAR_FILMS&page=1";
        this.API_URL_SEARCH = "https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword?keyword=";
        this.API_URL_MOVIE_DETAILS = "https://kinopoiskapiunofficial.tech/api/v2.2/films/"
    }

    async getMovies(url) {
        const resp = await fetch(url, {
          headers: {
            "Content-Type": "application/json",
            "X-API-KEY": this.API_KEY,
          },
        });
        return await resp.json();
    }


}

export {kinopoiskAPI};